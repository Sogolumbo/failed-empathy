# Weiterführende Informationen über diesen Vortrag

Hier sind Informationen über das autistische Spektrum und autistische Erfahrungen gesammelt, die zum Verständnis des vortrages beitragen können, jedoch auch Sachargumente in betalars'  Beschwerde an den Rundfunkrat wissenschaftlich belegen.

## Wissenschaftliche Quellen
Hier sind wissenschaftliche Quellen aufgeführt, die wesentliche Aussagen der Serie "Ella Schön" wiederlegen.

Das Dokument wird in der nächsten Zeit noch aktualisiert werden.

### Autistische Empathie
In der Serie Ella Schön wird immer wieder dargestellt, wie die Figur Ella Schön die körperliche und mentale Gesundheit ihrer Mitmenschen gefährdet, weil sie unwillig ist, ihnen gegenüber empatisch zu sein. Und das, obwohl sie in der Lage ist, sich in andere hinain zu versetzen ... insbesondere, wenn es ihr zum eigenne Vorteil dient.

Das in der Serie dargestellte Verhalten wäre jedoch symptomatisch für eine antisoziale Persönlichkeitsstörung.

In dem Paper [Autismus oder „Psychopathy“?](https://link.springer.com/article/10.1007/s11757-013-0232-5) untersuchen
Jens Roberz, Gerd Lehmkuhl und Kathrin Sevecke diese Zusammenhänge zwischen autistishcen Menschen und antisozialen Verhalten.

Sie stellen dabei fest, dass es vorkommen kann, dass autistische Menschen auch psychiopatische Persönlichkeittstörungen aufweisen, dies aber als comorbid zu betrachten seien.

[Denn autistishce Menschen haben ein großes Bedürfnis empatisch zu sein und wollen auch Empathie zeigen](https://www.spectrumnews.org/opinion/viewpoint/people-with-autism-can-read-emotions-feel-empathy/) ... ihre Schwierigkeiten in dem Bereich liegen jedoch vor allem daran, dass es ihnen schwer fällt, sich in die Gefühls- und Gedankenwelt anderer hineinzuversetzen.

[I care for your says the autistic moral brain.](https://www.sciencedaily.com/releases/2016/03/160329101041.htm)

Außerdem wird dargestellt, dass Ella Schön gegenüber Christina, einer werdenden Mutter, immer wieder gegen den ausdrücklichen Wunsch in die Wohnung von ihr eindringt und außerdem versucht, juristische Gewalt gegen sie durch zu setzen.

Dies repliziert zweifellos Gesellschaftlich weit verbreitete Vorurteile darüber, dass autistische Menschen gibt und hier zeigen Studien: unter Medienwirksamen Gewallttaten sind autistische Menschen häufiger: [Dies wird in diesem Paper auch dokumentiert](https://www.tandfonline.com/doi/pdf/10.1080/00223980.2016.1175998?needAccess=true)

Allerdings ist das nicht repräsentativ für die meisten autistische Wesen und allgemein gilt, dass autistische Menschen viel häufiger als normal Opfer von Gewalt und Mobbing werden:
 - [Autistic children experience significantly more adverse childhood experiences than their neurotypical peers](https://link.springer.com/article/10.1007%2Fs10803-016-2905-3)
 - [Children with ASD are bullied by peers at a rate 3-4 times that of non-disabled peers](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6082373/)

Erschwerend hinzu kommt, dass autistische Menschen zudem auch noch [viel seltener professionelle Hilfe bekommen, wenn sie diese bebötigen.](https://link.springer.com/article/10.1007/s10803-018-3862-9)

### Autismus und Masking
Ella Schön ist eine autistische Frau, die bemerkenswerte Kognitive Fähigkeiten hat, jedoch kaum Emotionen zeigt und auch in einer ständigen körperlichen Anstrengung lebt: Was das ZDF nicht verstanden hat ist, dass dieses Verhalten keinesfalls normale und gesunde autistische Emotionaliät darstellt, sondern eine Extremform von Masking.

Masking ist ein häufiger Coping-Mechanismus unter marginalisierten Menschen, bei dem Maskierende ihre natürlichen Emotionen verstecken, um so Ausgrenzung vorzubeugen.

Das ist eine teilweise erfolgreiche Strategie, ist aber auf lange Sicht sehr Kraftraubend, ungesund und trägt dazu bei, dass viele autistische Frauen sehr spät, gar nicht oder falsch diagnostiziert werden.

- [Social Camouflaging in Females with Autism Spectrum Disorder:](https://link.springer.com/content/pdf/10.1007/s10803-020-04695-x.pdf)
- [Linguistic camouflage in girls with autism
spectrum disorder](https://link.springer.com/content/pdf/10.1186/s13229-017-0164-6.pdf)
